/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;

import KeywordDrivenTestFramework.Core.BaseClass;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import org.junit.Test;

/**
 *
 * @author dluis
 */
public class SuiteCRM_TestSuite extends BaseClass
{

    static TestMarshall instance;
    public static Enums.DeviceConfig test;

    public SuiteCRM_TestSuite()
    {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentDevice = Enums.Device.Emulator;
        TestMarshall.currentDeviceConfig = Enums.DeviceConfig.SuiteCRM;
        TestMarshall.currentEnvironment = Enums.Environment.TEST;
    }

    @Test
    public void GMAILTEST() throws FileNotFoundException
    {
        requiresBrowser = false;
        Narrator.logDebug("SuiteCRM - Scenario 1  - Test Pack");
        instance = new TestMarshall("TestPacks\\ExactCRM.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
}
