/*
 * Changed made by James Joubert
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.browserType;
import static KeywordDrivenTestFramework.Core.BaseClass.DataBaseInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.inputFilePath;
import static KeywordDrivenTestFramework.Core.BaseClass.reportDirectory;
import static KeywordDrivenTestFramework.Core.BaseClass.reportGenerator;
import static KeywordDrivenTestFramework.Core.BaseClass.testCaseId;
import static KeywordDrivenTestFramework.Core.BaseClass.testDataList;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Reporting.ReportGenerator;
import KeywordDrivenTestFramework.Reporting.TestReportEmailerUtility;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.TestClasses.*;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import KeywordDrivenTestFramework.Utilities.AutoItDriverUtility;
import KeywordDrivenTestFramework.Utilities.CSVReportUtility;
import KeywordDrivenTestFramework.Utilities.DataBaseUtility;
import KeywordDrivenTestFramework.Utilities.ExcelReaderUtility;
import KeywordDrivenTestFramework.Utilities.PerformanceMonitor;
import KeywordDrivenTestFramework.Utilities.SeleniumDriverUtility;
import com.google.common.collect.ImmutableSet;
import com.google.common.reflect.ClassPath;
import com.google.common.reflect.ClassPath.ClassInfo;
import com.relevantcodes.extentreports.ExtentReports;
import java.io.*;
import static java.lang.System.err;
import static java.lang.System.out;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Stream;
import org.apache.tools.ant.util.ClasspathUtils;

/**
 *
 * @author fnell
 * @editir jjoubert
 */
public class TestMarshall extends BaseClass
{

    // Handles calling test methods based on test parameters , instantiates Selenium Driver object   
    ExtentReports extentReports;
    ExcelReaderUtility excelInputReader;
    CSVReportUtility cSVReportUtility;
    TestReportEmailerUtility reportEmailer;
    //James removed(Moved to Narrator)
//    PrintStream errorOutputStream;
//    PrintStream infoOutputStream;
    private String dateTime;
    private int totalIgnore = 0;
    
    

    public TestMarshall()
    {
        inputFilePath = ApplicationConfig.InputFileName();
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        browserType = ApplicationConfig.SelectedBrowser();
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        DataBaseInstance = new DataBaseUtility();
        AutoItInstance = new AutoItDriverUtility();
        performanceMonitor = new PerformanceMonitor();
        
        this.generateReportDirectory();

    }

    public TestMarshall(String inputFilePathIn)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        browserType = ApplicationConfig.SelectedBrowser();
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        DataBaseInstance = new DataBaseUtility();
        AutoItInstance = new AutoItDriverUtility();
        performanceMonitor = new PerformanceMonitor();
        
        this.generateReportDirectory();

    }

    public TestMarshall(String inputFilePathIn, Enums.BrowserType browserTypeOverride)
    {
        inputFilePath = inputFilePathIn;
        testDataList = new ArrayList<>();
        excelInputReader = new ExcelReaderUtility();
        cSVReportUtility = new CSVReportUtility(inputFilePath);
        cSVReportUtility.createCSVReportDirectoryAndFile();
        browserType = browserTypeOverride;
        reportGenerator = new ReportGenerator(inputFilePath, ApplicationConfig.ReportFileDirectory());
        SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
        DataBaseInstance = new DataBaseUtility();
        AutoItInstance = new AutoItDriverUtility();
        performanceMonitor = new PerformanceMonitor();
        
        this.generateReportDirectory();

    }

    public void runKeywordDrivenTests() throws FileNotFoundException
    {

        int numberOfTest = 0;

        testDataList = loadTestData(inputFilePath);

        if (testDataList.size() < 1)
        {
            Narrator.logError("Test data object is empty - spreadsheet not found or is empty");
        }
        else
        {
            //CREATE NEW REPORT FILE
            reportGenerator.creatingNewTextFile();
            //James added 
//            if (requiresBrowser == true)
//            {
//                CheckBrowserExists();
//            }
            Narrator.takeScreenShot(true, "init");
            // Each case represents a test keyword found in the excel spreadsheet
            for (TestEntity testData : testDataList)
            {
                numberOfTest++;
                testCaseId = testData.TestCaseId;
                // Make sure browser is not null - could have thrown an exception and terminated
                //James added // Steve edited
                if (requiresBrowser == true)
                {
                    CheckBrowserExists();
                }
                
                //Stops Running Appuim Server 
                 stopAppiumServerIfExists();
                 
                 
                 //Commented below
                 //performanceMonitor.startMonitoring("This Report was generated for the following scenario - " + resolveScenarioName());
                
                // Skip test methods and test case id's starting with ';'
                if (!testData.TestMethod.startsWith(";") && !testData.TestCaseId.startsWith(";"))
                {
                    Narrator.logDebug("Executing test - " + testData.TestMethod + " | " + numberOfTest + " of " + testDataList.size());
                    
                   
                    
                    try
                    {
                        ClassInfo testClassInfo  = getKeywordTestClass(testData.TestMethod);                    
                    
                        Class testClass = testClassInfo.load();
                        
                        Constructor constructor = testClass.getConstructor(TestEntity.class);
                        
                        Object testClassInstance = constructor.newInstance(testData);
                        
                        if(testClassInstance.getClass().getAnnotation(KeywordAnnotation.class).createNewBrowserInstance() && numberOfTest > 1)
                            ensureNewBrowserInstance();
                        
                        
                        // Set up the Performance Monitoring
                        performanceMonitor.newPage(testData.TestCaseId);
                                              
                        Method executeTestMethod = testClassInstance.getClass().getMethod("executeTest");

                        reportGenerator.addResult((TestResult) executeTestMethod.invoke(testClassInstance));    
                        
                        performanceMonitor.endPage();
                       
                    }
                    catch(Exception ex)
                    {
                        err.println("[ERROR] Critical Error during keyword execution - " + testData.TestMethod + " - error: " +ex.getMessage());
                        ex.printStackTrace();
                    }
                        
                    

                    reportTextFile(numberOfTest);
                    Narrator.logDebug("Continuing to next test method");
                }
                else
                {
                    totalIgnore++;
                    reportGenerator.writeToFile4(numberOfTest, testDataList.size(), totalIgnore);

                }
            }
            
            
             //Generate performance log report
             
             performanceMonitor.endMonitoring(); 
            
            if (SeleniumDriverInstance != null && SeleniumDriverInstance.isDriverRunning())
            {
                SeleniumDriverInstance.shutDown();
            }
//            if (AppiumDriverInstance != null && AppiumDriverInstance.isDriverRunning())
//            {
//                AppiumDriverInstance.shutDown();
//            }
            
             //Stops Running Appuim Server 
             stopAppiumServerIfExists();
             
            

            reportGenerator.generateTestReport();
            reportEmailer = new TestReportEmailerUtility(reportGenerator.testResults);
            reportEmailer.SendResultsEmail();

 
        }
    }

    public void stopAppiumServerIfExists()
    {
        if (AppiumDriverInstance != null && AppiumDriverInstance.isDriverRunning())
        {
            switch(currentPlatform)
            {
                case Android:
                {
                  AppiumDriverInstance.stopAndroidServer();
                  break;
                }
                case iOS:
                {
                    AppiumDriverInstance.stopIOSServer();
                    break;
                }
            }
        }
    }
    
    private List<TestEntity> loadTestData(String inputFilePath)
    {
        return excelInputReader.getTestDataFromExcelFile(inputFilePath);
    }

    public static void CheckBrowserExists()
    {
        if (SeleniumDriverInstance == null)
        {
            SeleniumDriverInstance = new SeleniumDriverUtility(browserType);
            SeleniumDriverInstance.startDriver();
        }

        if (!SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.startDriver();
        }
    }

    public static void ensureNewBrowserInstance()
    {
        if (SeleniumDriverInstance.isDriverRunning())
        {
            SeleniumDriverInstance.shutDown();
        }
        SeleniumDriverInstance.startDriver();
    }

    public String generateDateTimeString()
    {
        Date dateNow = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_hh-mm-ss");
        dateTime = dateFormat.format(dateNow).toString();
        return dateTime;
    }
    
    
    
    public ClassPath.ClassInfo getKeywordTestClass(String keywordName)
    {
       try
       {
           
            //Get list of all loaded classes for the package - defined at runtime - we need to be able to isolate just the TestClasses
            // in order to extract the one matching the keyword to be executed
            ClassPath classPath = ClassPath.from(TestMarshall.class.getClassLoader());                  
            
            //Next we set up Predicates (a type of query in java) to isolate the list of classes to only those pertaining to the framework
            // i.e. no dependencies included 
            ImmutableSet<ClassPath.ClassInfo> allClasses = classPath.getTopLevelClassesRecursive("KeywordDrivenTestFramework.Testing");
            
            //We then filter the classes to only those who have the required annotations - annotations used to add meta
            //data to the TestClasses sothat we can scan them to read their Keywords - this uses Lambda notation only available in Java 8 and above.
            Predicate<ClassPath.ClassInfo> hasAnnotationPredicate = c-> c.load().isAnnotationPresent(KeywordAnnotation.class);
            Stream<ClassPath.ClassInfo> annotatedClasses =  allClasses.stream().filter(hasAnnotationPredicate);
            
            //The filtered list is then queried a second time in order to retrieve the valid TestClass based on the keywordName
            Predicate<ClassPath.ClassInfo> checkKeywordPredicate = c-> c.load().getAnnotation(KeywordAnnotation.class).Keyword().equals(keywordName);
            ClassPath.ClassInfo  testClass = annotatedClasses.filter(checkKeywordPredicate).findFirst().get();
            
            if(testClass == null)
            {
                err.println("[ERROR] Failed to resolve TestClass for keyword - " + keywordName + " - error: Keyword not found");
            }
            
            return testClass;
         
       }
       catch(Exception ex)
       {
           err.println("[ERROR] Failed to resolve TestClass for keyword - " + keywordName + " - error: " + ex.getMessage());
           
           return null;
       }
    }

    public void generateReportDirectory()
    {
        reportDirectory = ApplicationConfig.ReportFileDirectory() + resolveScenarioName() + "_" + this.generateDateTimeString();
        String[] reportsFolderPathSplit = this.reportDirectory.split("\\\\");
        this.currentTestDirectory = ApplicationConfig.ReportFileDirectory() + "\\" + reportsFolderPathSplit[reportsFolderPathSplit.length - 1];
    }

    //James removed(Moved to Narrator)
//    public void redirectOutputStreams() {
//        try {
//            File reportDirectoryFile = new File(reportDirectory);
//            File errorFile = new File(reportDirectory + "\\" + "ErrorTestLog.txt");
//            File infoFile = new File(reportDirectory + "\\" + "InfoTestLog.txt");
//            File narrator = new File(reportDirectory + "\\Narrator_Log.txt");
//            reportDirectoryFile.mkdirs();
//
//            errorOutputStream = new PrintStream(errorFile);//errorFile
//            infoOutputStream = new PrintStream(infoFile);//infoFile
//
//            System.setOut(infoOutputStream);
//            
//            System.setErr(errorOutputStream);
//            
//        } catch (FileNotFoundException ex) {
//            Narrator.errorLog(" could not create log files - " + ex.getMessage());
//        }
//    }
    //James removed (Moved to Narrator)
//    public void flushOutputStreams() {
//
//        errorOutputStream.flush();
//        infoOutputStream.flush();
//        
//        errorOutputStream.close();
//        infoOutputStream.close();
//
//    }
    public void reportTextFile(int numberOfTests)
    {
        reportGenerator.writeToFile4(numberOfTests, testDataList.size(), totalIgnore);
    }
}
