/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.PageObjects;

import KeywordDrivenTestFramework.Core.BaseClass;

/**
 *
 * @author fnell
 */
public class ExactCRMPageObjects extends BaseClass
{  
    public static String GmailURL()
    {
        // Use ENUM
        return currentEnvironment.PageUrl;
    }

    public static String checkBoxXpath() 
    {
        return "//android.widget.CheckBox[@resource-id='com.exact.synergy.crm:id/checkBox']";
    }

    public static String useDemoXpath() 
    {
        return "//android.widget.Button[@resource-id='com.exact.synergy.crm:id/button_demo']";
    }

    public static String alertXpath() 
    {
        return "//android.widget.Button[@resource-id='android:id/button2']";
    }

    public static String gotItXpath() 
    {
        return "//android.widget.Button[@resource-id='com.exact.synergy.crm:id/agreeButton']";
    }
    
    public static String hamBurger()
    {
        return "//android.widget.ImageView[@android:id/up]";
    }
    
    public static String accountsButton()
    {
        return "//android.widget.TextView[@com.exact.synergy.crm:id/menuItemTitle]";
    }
    
    public static String addAccount()
    {
        return "//android.widget.TextView[@index='0']";
    }
    
    public static String name()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_name]";
    }
    
    public static String street()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_street]";
    }
    
    public static String postalCode()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_postal_code]";
    }
    
    public static String city()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_city]";
    }
    
    public static String country()
    {
        return "//android.widget.TextView[@com.exact.synergy.crm:id/txt_country]";
    }
    
    public static String state()
    {
        return "//android.widget.TextView[@com.exact.synergy.crm:id/txt_state]";
    }
    
    public static String southAfrica()
    {
        return "//android.widget.TextView[@text='South Africa']";
    }
    
    public static String westernCape()
    {
        return "//android.widget.TextView[@text='Western Cape']";
    }

    public static String telNumber()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_phone]";
    }
    
    public static String email()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_mail]";
    }
    
    public static String webAddress()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_web]";
    }
    
    public static String contactPerson()
    {
        return "//android.widget.TextView[@com.exact.synergy.crm:id/label_add_contact]";
    }
    
    public static String title()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_title]";
    }
    
    public static String Mr()
    {
        return "//android.widget.TextView[@text='Mr.']";
    }
    
    public static String firstName()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_first_name]";
    }
    
    public static String lastName()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_last_name]";
    }
    
    public static String jobDescription()
    {
        return "//android.widget.EditText[@com.exact.synergy.crm:id/txt_function]";
    }
    
    public static String tick()
    {
        return "//android.view.View[@index='0']"; 
    }
    
    public static String newAccount()
    {
        return "//android.widget.TextView[@android:id/action_bar_title]";
    }
    
    public static String contactsAdd()
    {
        return "//android.widget.TextView[@com.exact.synergy.crm:id/monitorText]";
    }
    
    public static String addContact()
    {
        return "//android.widget.TextView[@index='0']";
    }
    
    public static String accountName()
    {
        return "//android.widget.TextView[@text='tzawistowski']"; // convert to String
    }
    
    public static String account()
    {
        return "//android.widget.TextView[@android:id/action_bar_title]";
    }
    
    public static String contactName()
    {
        return "//android.widget.TextView[@text='James Peter']"; //convert to String
    }
    
    public static String validateContactName()
    {
        return "//android.widget.TextView[@text='James Peter']"; //convert to String
    }
    
    public static String validateAccountName()
    {
        return "//android.widget.TextView[@text='tzawistowski']"; //convert to String
    }
    
    public static String contact()
    {
        return "//android.widget.TextView[@android:id/action_bar_title]";
    }
    
    public static String Account()
    {
        return "//android.widget.TextView[@android:id/action_bar_title]";
    }
    
    
    
    
    
    
    
    
    
    
   
    
    //
}
