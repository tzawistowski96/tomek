/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SikuliDriverInstance;
import KeywordDrivenTestFramework.Entities.TestEntity;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Testing.PageObjects.ExactCRMPageObjects;
import KeywordDrivenTestFramework.Utilities.AppiumDriverUtility;
import KeywordDrivenTestFramework.Utilities.SikuliDriverUtility;

/**
 *
 * @author fnell
 */

@KeywordAnnotation
(
    Keyword = "ExactCRM_Activity",
    createNewBrowserInstance = false
)
public class LoginCRM extends BaseClass
{

    public
            
        TestEntity testData;
        String error = "";
        Narrator narrator;

    public LoginCRM(TestEntity testData)
    {
        this.testData = testData;
        narrator = new Narrator(testData);
        AppiumDriverInstance = new AppiumDriverUtility();
    }

    public TestResult executeTest()
    {
        // This step will Launch the browser and navigate to the GMail URL
        if (!Login())
        {
            return narrator.testFailed("Failed to navigate and Log into ExactCRM" + error);
        }
        
        if (!createMainAccount())
        {
            return narrator.testFailed("Failed to navigate to SuiteCRM app - " + error);
        }
        
        if (!createContacts())
        {
            return narrator.testFailed("Failed to navigate to SuiteCRM app - " + error);
        }
        
        if (!validateContacts())
        {
            return narrator.testFailed("Failed to navigate to SuiteCRM app - " + error);
        }
       
        // This step will sign into the specified gmail account with the provided credentials
        return narrator.finalizeTest("Successfully Navigated through SuiteCRM app");
    }

    public boolean Login()
    {
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.checkBoxXpath()))
        {
            error = "Failed to click on checkbox on the Login Page";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.useDemoXpath()))
        {
            error = "Failed to click on Demo button on the Login Page";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.alertXpath()))
        {
            error = "Failed to navigate to alert pop-up field";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.gotItXpath()))
        {
            error = "Failed to click on the 'Got it' button on the alert field";
            return false;
        }
        return true;
    }
            
    public boolean createMainAccount()
    {
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.hamBurger()))
        {
            error = "Failed to click on hamburger button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.accountsButton()))
        {
            error = "Failed to click on Accounts Button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.addAccount()))
        {
            error = "Failed to click on the add Account button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.name()))
        {
            error = "Failed to enter Account name";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.street()))
        {
            error = "Failed to enter street name";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.postalCode()))
        {
            error = "Failed to enter postal code";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.city()))
        {
            error = "Failed to enter city";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.country()))
        {
            error = "Failed to click on country field";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.southAfrica()))
        {
            error = "Failed to click on South Africa";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.state()))
        {
            error = "Failed to click on state field";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.westernCape()))
        {
            error = "Failed to click on Western Cape";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.telNumber()))
        {
            error = "Failed to enter the account's Telephone Number";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.email()))
        {
            error = "Failed to enter the account's email";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.webAddress()))
        {
            error = "Failed to enter the account's Web Address";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.contactPerson()))
        {
            error = "Failed to click on add Main Contact button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.title()))
        {
            error = "Failed to click on title field";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.Mr()))
        {
            error = "Failed to click on Mr. as title";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.firstName()))
        {
            error = "Failed to enter first name";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.lastName()))
        {
            error = "Failed to enter last name";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.jobDescription()))
        {
            error = "Failed to enter Job Description";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.email()))
        {
            error = "Failed to enter contact's email";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.telNumber()))
        {
            error = "Failed to enter contact's telephone number";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.tick()))
        {
            error = "Failed to save main contact";
            return false;
        }

        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.tick()))
        {
            error = "Failed to save Account";
            return false;
        }
        return true;
    }
    
    public boolean createContacts()
    {
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.accountName()))
        {
            error = "Failed to click on Account 'tzawistowski'";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.contactsAdd()))
        {
            error = "Failed to click on the account's contact icon";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.addContact()))
        {
            error = "Failed to click on the add contact to account button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.title()))
        {
            error = "Failed to click on title field";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.Mr()))
        {
            error = "Failed to click on Mr. as title";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.firstName()))
        {
            error = "Failed to enter first name";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.lastName()))
        {
            error = "Failed to enter last name";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.jobDescription()))
        {
            error = "Failed to enter Job Description";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.email()))
        {
            error = "Failed to enter contact's email";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.telNumber()))
        {
            error = "Failed to enter contact's telephone number";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.tick()))
        {
            error = "Failed to save main contact";
            return false;
        }
  
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.newAccount()))
        {
            error = "Failed to click on New Account";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.account()))
        {
            error = "Failed to click on Account Button";
            return false;
        }
        return true;
    }
    
    public boolean validateContacts()
    {
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.accountName()))
        {
            error = "Failed to click on Account 'tzawistowski'"; //convert to String
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.contactsAdd()))
        {
            error = "Failed to click on the account's contact icon";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.accountName()))
        {
            error = "Failed to click on specific contact";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.validateContactName()))
        {
            error = "Failed to validate contact";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.validateAccountName()))
        {
            error = "Failed to validate account";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.contact()))
        {
            error = "Failed to click on the contact button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.contact()))
        {
            error = "Failed to click on the contacts button";
            return false;
        }
        
        if(!AppiumDriverInstance.clickElementbyId(ExactCRMPageObjects.Account()))
        {
            error = "Failed to click on the account button";
            return false;
        }
        return true;
    }
    
}
